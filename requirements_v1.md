# Requirements

Allowed (mutually married, single):

```dot
digraph G {
    rankdir="TB"
    B -> A
    A -> B
    C
    { rank=same; A; B; C }
}
```

Disallowed (self-married):

```dot
digraph G {
    D -> D
}
```
