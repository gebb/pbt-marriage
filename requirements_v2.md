# Requirements

Allowed (mutually married, single):

```dot
digraph G {
    rankdir="TB"
    B -> A
    A -> B
    C
    { rank=same; A; B; C }
}
```

Disallowed (self-married):

```dot
digraph G {
    D -> D
}
```

Disallowed (non-mutual):

```dot
digraph G {
    rankdir="LR"

    C -> B
    B -> A
    A -> B
    E -> F
    A -> E [style=invis]

    { rank=same; A; B }
    { rank=same; E; F }
}
```
