using System;
using Xunit;

namespace Marriage.Tests
{
    public class PersonTest
    {
        private Person _a = new Person { Name = "a" };
        private Person _b = new Person { Name = "b" };

        [Fact]
        public void Cannot_marry_self()
        {
            Assert.Throws<ArgumentException>(
                "newSpouse",
                () => _a.Marry(_a));
        }

        [Fact]
        public void Spouse_relation_is_mutual()
        {
            _a.Marry(_b);
            Assert.Equal(_b, _a.Spouse);
            Assert.Equal(_a, _b.Spouse);
        }

        [Fact]
        public void Cannot_marry_if_already_married()
        {
            Person _c = new Person { Name = "c" };
            _a.Marry(_b);

            Assert.Throws<InvalidOperationException>(
                () => _a.Marry(_c));
        }

        [Fact]
        public void Cannot_marry_if_new_spouse_is_married()
        {
            Person _c = new Person { Name = "c" };
            _a.Marry(_b);

            Assert.Throws<ArgumentException>(
                "newSpouse",
                () => _c.Marry(_a));
        }
    }
}
