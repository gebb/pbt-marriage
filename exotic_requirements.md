# Requirements

Disallowed (cycle of four):

```dot
digraph G {
    layout="circo"

    A -> B
    B -> C
    C -> D
    D -> A

}
```
