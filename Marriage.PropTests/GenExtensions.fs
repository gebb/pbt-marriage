module Hedgehog.Gen

  let sequence<'a> (cs: List<Gen<'a>>) : Gen<List<'a>> =
    let mcons p q = gen {
      let! x = p
      let! y = q
      return (x::y)
    }
    let start : Gen<List<'a>> = Gen.constant []
    List.foldBack mcons cs start

  /// Monadic fold over the elements of a list,
  /// associating to the right, i.e. from right to left.
  let foldlM<'a, 'b>
    (folder: 'b -> 'a -> Gen<'b>)
    (state: 'b)
    (xs: List<'a>)
    : Gen<'b> =
      let combine x (genState: 'b -> Gen<'b>) currState = Gen.bind (folder currState x) genState
      List.foldBack combine xs Gen.constant state

  /// Monadic fold over the elements of a list,
  /// associating to the left, i.e. from left to right.
  let foldrM<'a, 'b>
    (folder: 'a -> 'b -> Gen<'b>)
    (state: 'b)
    (xs: List<'a>)
    : Gen<'b> =
      let combine (genState: 'b -> Gen<'b>) x currState = Gen.bind (folder x currState) genState
      List.fold combine Gen.constant xs state

(*

Haskell uses foldlM as the monadic fold, so we should too.

Examples of foldlM and foldrM (executed in ghci):

Prelude Data.Foldable> import Data.Foldable
Prelude Data.Foldable> :{
Prelude Data.Foldable| foldlM (\str n ->
Prelude Data.Foldable|                  (putStrLn $ "Repeating string " ++ (show str) ++ " "
Prelude Data.Foldable|                   ++ (show n) ++ " times")
Prelude Data.Foldable|                  >> (return $ "(" ++ (concat $ replicate n str) ++ ")" )
Prelude Data.Foldable|        ) "x" [2,3,4]
Prelude Data.Foldable| :}
Repeating string "x" 2 times
Repeating string "(xx)" 3 times
Repeating string "((xx)(xx)(xx))" 4 times
"(((xx)(xx)(xx))((xx)(xx)(xx))((xx)(xx)(xx))((xx)(xx)(xx)))"
Prelude Data.Foldable> :{
Prelude Data.Foldable| foldrM (\n str ->
Prelude Data.Foldable|                  (putStrLn $ "Repeating string " ++ (show str) ++ " "
Prelude Data.Foldable|                   ++ (show n) ++ " times")
Prelude Data.Foldable|                  >> (return $ "(" ++ (concat $ replicate n str) ++ ")" )
Prelude Data.Foldable|        ) "x" [2,3,4]
Prelude Data.Foldable| :}
Repeating string "x" 4 times
Repeating string "(xxxx)" 3 times
Repeating string "((xxxx)(xxxx)(xxxx))" 2 times
"(((xxxx)(xxxx)(xxxx))((xxxx)(xxxx)(xxxx)))"

foldM from Control.Monad:

Prelude> import Control.Monad
Prelude Control.Monad> :{
Prelude Control.Monad| foldM (\str n ->
Prelude Control.Monad|                  (putStrLn $ "Repeating string " ++ (show str) ++ " "
Prelude Control.Monad|                   ++ (show n) ++ " times")
Prelude Control.Monad|                  >> (return $ "(" ++ (concat $ replicate n str) ++ ")" )
Prelude Control.Monad|        ) "x" [2,3,4]
Prelude Control.Monad| :}
Repeating string "x" 2 times
Repeating string "(xx)" 3 times
Repeating string "((xx)(xx)(xx))" 4 times
"(((xx)(xx)(xx))((xx)(xx)(xx))((xx)(xx)(xx))((xx)(xx)(xx)))"

*)

  /// Monadic fold over the elements of a list.
  /// 
  ///**Type parameters**
  /// 
  /// - `'a`: list element type.
  /// - `'b`: state type.
  /// 
  ///**Parameters**
  /// 
  /// - `folder`: the function to apply to the current state and list element.
  ///   Should return a generator of the next state.
  /// - `state`: the initial state, e.g. an empty list (or set, database, etc.).
  /// - `xs`: the input list.
  /// 
  ///**Returns**
  /// The generator of values of type `'b`.
  let foldM
    (folder: 'b -> 'a -> Gen<'b>)
    (state: 'b)
    (xs: List<'a>)
    : Gen<'b> = foldlM folder state xs