﻿module Tests
open Expecto
open Hedgehog
open Marriage

/// A command with arguments that is planned to be executed.
type Command =
  | Marry of Spouse1: string * Spouse2: string

/// Generates random commands.
module Gen =
  /// A command without arguments, placeholder for a future `Command` or a sequence of `Command`'s.
  type Cmd =
    | CmMarry

  let names: Gen<Set<string>> =
    Gen.int (Range.linear 2 50)
    |> Gen.map (fun size ->
      Array.init size id
      |> Array.map string
      |> Set.ofArray)

  let commands (people: Set<string>) : Gen<Command[]> = gen {
    let! cms = Gen.list (Range.linear 0 50) (Gen.constant Cmd.CmMarry)
    let nextCommand (soFar: List<Command>, unmarried: Set<string>) (c: Cmd) = gen {
      if Set.count unmarried < 2 then
        return (soFar, unmarried)
      else
        // Consider only unmarried candidates to satisfy preconditions.
        let! p1 = Gen.item unmarried
        let rest = unmarried |> Set.remove p1
        let! p2 = Gen.item rest
        let newUnmarried = rest |> Set.remove p2
        let nextCmd = Marry (p1, p2)
        return (nextCmd :: soFar, newUnmarried)
    }
    let! comms, _ = Gen.foldM nextCommand ([], people) cms
    return comms |> List.rev |> List.toArray
  }


open Tools

let makePerson (name: string) : Person =
  let p = Person ()
  p.Name <- name
  p

let run (names: Set<string>) (cmds: Command[]): Person[] =
  // Create a new set of people from scratch at a late stage (during run),
  // because we mutate them, which is incompatible with Hedgehog.
  let people =
    names
    |> Seq.map (fun name -> name, makePerson name)
    |> dict
  cmds
  |> Array.iter (
    function
    | Marry (p1, p2) -> people.[p1].Marry(people.[p2])
  )
  people.Values |> Seq.toArray

let spouseAsString (p: Person) = if p.IsMarried then $"{p.Spouse}" else "N/A"

[<Tests>]
let marriageTests = testList "Marriage" [
  // This property is stronger than
  // "At most one person considers a person their spouse"
  testCase "Spouse relation is mutual" <| fun () ->
    Property.check <| property {
      let! names = Gen.names
      let! cmds = Gen.commands names
      let people = run names cmds

      let isMutual (x: Person) (spouse: Person) = spouse.Spouse = x
      let nonMutual =
        people
        |> Seq.filter (fun x -> x.IsMarried)
        |> Seq.map (fun x -> x, x.Spouse)
        |> Seq.filter (fun (x,y) -> not <| isMutual x y)
        |> Seq.map (fun (x,y) -> $"{x} -> {y} -> {spouseAsString y}")
        |> Seq.toArray

      return! invariant [
        <@ nonMutual.Length = 0 @>
      ]
    }

  testCase "No self-marriage allowed" <| fun () ->
    Property.check <| property {
      let! names = Gen.names
      let! cmds = Gen.commands names
      let people = run names cmds

      let selfMarried =
        people
        |> Seq.filter (fun x -> x.IsMarried && x.Spouse = x)
        |> Seq.toArray

      return! invariant [
        <@ selfMarried.Length = 0 @>
      ]
    }
]