﻿using System.Collections.Generic;

namespace Marriage
{
    public class PersonService
    {
        public readonly List<Person> People = new List<Person>();

        public void Add(Person p) => People.Add(p);
    }
}
