﻿using System;

namespace Marriage
{
    public class Person
    {
        public string Name { get; set; }
        public Person Spouse { get; private set; }
        public bool IsMarried => Spouse != null;

        public void Marry(Person newSpouse)
        {
            if (this == newSpouse)
            {
                throw new ArgumentException(
                    "Cannot marry self",
                    nameof(newSpouse));
            }

            if (this.IsMarried)
            {
                throw new InvalidOperationException(
                    "The person is already married");
            }

            if (newSpouse.IsMarried)
            {
                throw new ArgumentException(
                    "The new spouse is already married",
                    nameof(newSpouse));
            }

            newSpouse.Spouse = this;
            Spouse = newSpouse;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
