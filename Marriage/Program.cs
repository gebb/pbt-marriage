﻿using System;

namespace Marriage
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new Person { Name = "a" };
            var b = new Person { Name = "b" };
            b.Marry(a);
            Console.WriteLine($"Equal: {a == b}");
        }
    }
}
